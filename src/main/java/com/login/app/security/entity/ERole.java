package com.login.app.security.entity;

public enum ERole {
	ROLE_USER, ROLE_MODERATOR, ROLE_ADMIN
}
